Substrateapp::Application.routes.draw do
  # site root
  root :to => 'pages#landing'

  # default catch-all route
  get "/:action", :controller => "pages"
end
