# config valid only for Capistrano 3.1
lock '3.2.0'

set :stages, %w{production}
# set :default_stage, "staging"
set :application, 'cru00020'
set :repo_url, 'git@bitbucket.org:commix/substrate-rails-app.git'
# set :branch, "0.2.0-features-wip"

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

# ------------------------------------
# GIT and SSH
# ------------------------------------

set :user, 'crushfest'
set :ssh_options, {
  user: fetch(:user),
  # forward_agent: true,
  keys: ["~/.ssh/commix.pem"]
  # verbose: :debug
}

# Set up rake and rails commands
SSHKit.config.command_map[:rake]  = "bundle exec rake"
SSHKit.config.command_map[:rails] = "bundle exec rails"

# ------------------------------------
# Linked Directories
# ------------------------------------
set :linked_files, %w{config/database.yml config/unicorn.rb config/settings.yml}
set :linked_dirs, %w{bin log tmp vendor/bundle public/system}

# set target folder
set :app_base_path, "/srv/www"
set :app_path, fetch(:deploy_to)

# ------------------------------------
# HOOKS
# ------------------------------------

# automatic git push before deploy
before "deploy:started", "deploy:push"
after "deploy:publishing", "deploy:migrate"

# create tmp dir
# before "deploy:publishing", "deploy:dbmigrate"

#  RESTART UNICORN using capistrano3/unicorn
after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:stop'
    invoke 'unicorn:reload'
    invoke 'unicorn:start'
    # invoke 'unicorn:restart'
  end
end